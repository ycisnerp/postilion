package RunScripts;

import Globales.Reporte;
import Globales.Util;
import TestPages.Login;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunLogin {
    @Before
    public void iniciar_Chrome() {
        Util.Inicio("RunLogin");
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Navegador: Chrome; " + " Version: 88.0.4324.190 (Official Build) (64-bit)"  );
    }

    @Test
    public void login(){
        Login ingresar = new Login();
        ingresar.click_btn_confg();
        ingresar.click_btn_link();
    }

    @After
    public void salir()
    {
        Reporte.finReporte();
    }
}
