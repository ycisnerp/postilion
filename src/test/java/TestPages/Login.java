package TestPages;

import Globales.Reporte;
import Globales.Util;
import org.junit.After;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {
    public Login() {
        PageFactory.initElements(Util.driver, this);
    }


    @FindBy(id="proceed-link")
    WebElement btn_link = null;

    @FindBy(id="details-button")
    WebElement btn_configuracion = null;

    public void click_btn_confg()
    {
        String actual = btn_configuracion.getText();
        Util.assert_contiene("LOGIN", "Click en bot�n", actual,"Configuraci�n avanzada", true, "N");
        btn_configuracion.click();
    }

    public void click_btn_link()
    {
        String actual = btn_link.getText();
        Util.assert_contiene("LOGIN", "Click en bot�n", actual,"Continuar a", true, "N");
        btn_link.click();
    }

}
